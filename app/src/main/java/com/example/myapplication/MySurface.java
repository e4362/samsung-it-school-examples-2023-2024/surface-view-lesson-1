package com.example.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

public class MySurface extends SurfaceView implements
        SurfaceHolder.Callback {
    float x, y; //координаты картинки
    float tx, ty; //координаты точки касания
    float dx, dy; //смещение
    float koeff; //коэффициент смещения

    Bitmap image;
    Resources res;
    Paint paint;

    DrawThread drawThread;

    public MySurface(Context context) {
        super(context);
        getHolder().addCallback(this);
        x = 100;
        y = 100;
        koeff = 20;
        res = getResources();
        image = BitmapFactory.decodeResource(res, R.drawable.apple);
        paint = new Paint();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            tx = event.getX();
            ty = event.getY();
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(image, x, y, paint);
        if (tx != 0){
            calculate();
        }
        x += dx;
        y += dy;
    }

    private void calculate(){
        double g = Math.sqrt((tx - x)*(tx - x) + (ty - y)*(ty - y));
        dx = (float) (koeff * (tx - x) / g);
        dy = (float) (koeff * (ty - y) / g);
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        drawThread = new DrawThread(this, getHolder());
        drawThread.isRun = true;
        drawThread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        boolean stop = true;
        drawThread.isRun = false;
        while (stop){
            try {
                drawThread.join();
                stop = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
